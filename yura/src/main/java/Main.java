import org.apache.commons.lang3.StringUtils;
import java.io.*;
public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter exponent of number 2 from 2 to 7: ");
        int exponent = Integer.parseInt(br.readLine());
        drawCross(exponent);
    }
    private static void drawCross(int exponent) {
        final int MIN_SIZE = 2;
        int size = (int) Math.pow(MIN_SIZE, exponent);
        int spacesBeforeAndAfterSymbol = 0;
        int numberOfSymbols = 1;
        int spacesBetweenSymbol = (int) ((size) - Math.pow(MIN_SIZE, exponent/MIN_SIZE));
        while (spacesBetweenSymbol >= 0) {
            drawLine(spacesBeforeAndAfterSymbol, numberOfSymbols, spacesBetweenSymbol);
            spacesBeforeAndAfterSymbol++;
            numberOfSymbols++;
            spacesBetweenSymbol = spacesBetweenSymbol - 2;
        }
        while (spacesBeforeAndAfterSymbol >= 0) {
            spacesBeforeAndAfterSymbol--;
            numberOfSymbols--;
            spacesBetweenSymbol = spacesBetweenSymbol + 2;
            drawLine(spacesBeforeAndAfterSymbol, numberOfSymbols, spacesBetweenSymbol);
        }
    }
    private static void drawLine(int spacesBeforeSymbol, int numberOfSymbols, int spacesBetweenSymbol) {
        String summary = "%s%s%s%s%s%s";
        String empty = " ";
        String symbol = "#";
        String x = StringUtils.repeat(empty, spacesBeforeSymbol);
        String y = StringUtils.repeat(symbol, numberOfSymbols);
        String z = StringUtils.repeat(empty, spacesBetweenSymbol);
        System.out.println(String.format(summary, x, y, z, z, y, x));
    }
}
