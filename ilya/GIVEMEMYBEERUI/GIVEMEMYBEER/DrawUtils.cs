﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GIVEMEMYBEER
{
    public class DrawUtils
    {
        public static string Draw(int fieldDimX, int fieldDimY, int startWidth, int shift, int increase,
            bool reflectionVertical, bool reflectionHorizontal, int startX, int startY, int endX, int endY, bool directionX, bool directionY)
        {
            //draw only part of field if some reflection are present
            //calculate part dimentions
            int partDimX = fieldDimX / (reflectionVertical ? 2 : 1);
            int partDimY = fieldDimY / (reflectionHorizontal ? 2 : 1);

            //chars field initialization
            char[,] field = new char[fieldDimY, fieldDimX];
            for (int y = 0; y < field.GetLength(0); y++)
            {
                for (int x = 0; x < field.GetLength(1); x++)
                {
                    field[y, x] = '_';
                }
            }

            //additional parameters to control curve
            bool cantCurve = false;
            bool hasToCurve = false;
            int lastX = startX;

            //cycles parameters are correlated to draw directions
            for (int y = startY; GetCycleCondition(directionY, y, fieldDimY) && y < partDimY && (directionY ? y <= endY : y > endY); y = GetUpdatedIterator(directionY, y))
            {
                List<int> drawRange = new List<int>();

                //drawing figure can't be curved if end point and shift are not correlated
                //in this case figure will not reach end point (will be close as it possible)
                if (!cantCurve)
                {
                    cantCurve = y == startY && Math.Abs(endX - lastX) >= Math.Abs(endY - y);
                }
                //should we change direction according to end point and shift 
                if (!cantCurve && !hasToCurve)
                {
                    hasToCurve = Math.Abs(endX - (directionX ? lastX + shift : lastX - shift)) > Math.Abs(endY - y);
                }

                for (int x = startX; GetCycleCondition(directionX, x, fieldDimX) && x < partDimX; x = GetUpdatedIterator(directionX, x))
                {
                    //should we start to draw a line?
                    bool isStartToDrawPoint = hasToCurve
                        ? Math.Abs(endX - x) == Math.Abs(endY - y) * shift
                        : Math.Abs(startX - x) == Math.Abs(startY - y) * shift;
                    if (isStartToDrawPoint)
                    {
                        bool hasToIncrease = increase != 0;
                        //where start to draw a line
                        var startDrawPoint = hasToCurve
                            ? Math.Abs(endX - Math.Abs(endY - y) * shift - (startWidth - 1))
                            : directionX ? x : x - Math.Abs(startY - y) * increase - (startWidth - 1);
                        //increase line length if needed
                        int drawLength = hasToIncrease ? increase * Math.Abs(startY - y) + startWidth : startWidth;
                        //range to draw for current line
                        drawRange = Enumerable.Range(startDrawPoint, drawLength).ToList();

                        lastX = startDrawPoint;
                    }
                    //fill point
                    field[y, x] = drawRange.Contains(x) ? '#' : '_';

                    //reflect point for each part if needed
                    if (reflectionVertical) field[y, fieldDimX - 1 - x] = field[y, x];
                    if (reflectionHorizontal) field[fieldDimY - 1 - y, x] = field[y, x];
                    if (reflectionHorizontal && reflectionVertical) field[fieldDimY - 1 - y, fieldDimX - 1 - x] = field[y, x];
                }
            }

            return ConvertArrayToString(field);
        }

        private static int GetUpdatedIterator(bool direction, int i)
        {
            return direction ? ++i : --i;
        }

        private static bool GetCycleCondition(bool direction, int i, int dimension)
        {
            return direction ? i < dimension : i >= 0;
        }

        private static string ConvertArrayToString(char[,] array)
        {
            string result = string.Empty;
            for (int y = 0; y < array.GetLength(0); y++)
            {
                for (int x = 0; x < array.GetLength(1); x++)
                {
                    result += array[y, x];
                }
                result += "\n";
            }
            return result;
        }
    }
}