﻿namespace GIVEMEMYBEER
{
    partial class DrawForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.fieldDimX = new System.Windows.Forms.TrackBar();
            this.labelFieldDimX = new System.Windows.Forms.Label();
            this.labelFieldDimY = new System.Windows.Forms.Label();
            this.fieldDimY = new System.Windows.Forms.TrackBar();
            this.startWidth = new System.Windows.Forms.TrackBar();
            this.labelStartWidth = new System.Windows.Forms.Label();
            this.labelStartWidthValue = new System.Windows.Forms.Label();
            this.labelShiftValue = new System.Windows.Forms.Label();
            this.labelShift = new System.Windows.Forms.Label();
            this.shift = new System.Windows.Forms.TrackBar();
            this.labelIncreaseValue = new System.Windows.Forms.Label();
            this.labelIncrease = new System.Windows.Forms.Label();
            this.increase = new System.Windows.Forms.TrackBar();
            this.checkBoxVertical = new System.Windows.Forms.CheckBox();
            this.checkBoxHorizontal = new System.Windows.Forms.CheckBox();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelXDimension = new System.Windows.Forms.Label();
            this.labelYDimension = new System.Windows.Forms.Label();
            this.startX = new System.Windows.Forms.TrackBar();
            this.labelStartX = new System.Windows.Forms.Label();
            this.labelStartXValue = new System.Windows.Forms.Label();
            this.labelEndXValue = new System.Windows.Forms.Label();
            this.labelEndX = new System.Windows.Forms.Label();
            this.endX = new System.Windows.Forms.TrackBar();
            this.endY = new System.Windows.Forms.TrackBar();
            this.labelEndY = new System.Windows.Forms.Label();
            this.labelEndYValue = new System.Windows.Forms.Label();
            this.labelStartYValue = new System.Windows.Forms.Label();
            this.labelStartY = new System.Windows.Forms.Label();
            this.startY = new System.Windows.Forms.TrackBar();
            this.radioButtonXLeft = new System.Windows.Forms.RadioButton();
            this.radioButtonXRight = new System.Windows.Forms.RadioButton();
            this.radioButtonYUp = new System.Windows.Forms.RadioButton();
            this.radioButtonYDown = new System.Windows.Forms.RadioButton();
            this.groupBoxDirectionX = new System.Windows.Forms.GroupBox();
            this.checkBoxXAuto = new System.Windows.Forms.CheckBox();
            this.groupBoxDirectionY = new System.Windows.Forms.GroupBox();
            this.checkBoxYAuto = new System.Windows.Forms.CheckBox();
            this.groupBoxReflection = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.fieldDimX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldDimY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.increase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startY)).BeginInit();
            this.groupBoxDirectionX.SuspendLayout();
            this.groupBoxDirectionY.SuspendLayout();
            this.groupBoxReflection.SuspendLayout();
            this.SuspendLayout();
            // 
            // fieldDimX
            // 
            this.fieldDimX.Location = new System.Drawing.Point(703, 12);
            this.fieldDimX.Maximum = 28;
            this.fieldDimX.Minimum = 7;
            this.fieldDimX.Name = "fieldDimX";
            this.fieldDimX.Size = new System.Drawing.Size(262, 45);
            this.fieldDimX.TabIndex = 0;
            this.fieldDimX.Value = 7;
            this.fieldDimX.Scroll += new System.EventHandler(this.fieldDimX_Scroll);
            // 
            // labelFieldDimX
            // 
            this.labelFieldDimX.AutoSize = true;
            this.labelFieldDimX.Location = new System.Drawing.Point(971, 12);
            this.labelFieldDimX.Name = "labelFieldDimX";
            this.labelFieldDimX.Size = new System.Drawing.Size(19, 13);
            this.labelFieldDimX.TabIndex = 1;
            this.labelFieldDimX.Text = "14";
            // 
            // labelFieldDimY
            // 
            this.labelFieldDimY.AutoSize = true;
            this.labelFieldDimY.Location = new System.Drawing.Point(1092, 379);
            this.labelFieldDimY.Name = "labelFieldDimY";
            this.labelFieldDimY.Size = new System.Drawing.Size(13, 13);
            this.labelFieldDimY.TabIndex = 3;
            this.labelFieldDimY.Text = "8";
            // 
            // fieldDimY
            // 
            this.fieldDimY.Location = new System.Drawing.Point(1094, 114);
            this.fieldDimY.Maximum = 16;
            this.fieldDimY.Minimum = 4;
            this.fieldDimY.Name = "fieldDimY";
            this.fieldDimY.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.fieldDimY.Size = new System.Drawing.Size(45, 262);
            this.fieldDimY.TabIndex = 2;
            this.fieldDimY.Value = 4;
            this.fieldDimY.Scroll += new System.EventHandler(this.fieldDimY_Scroll);
            // 
            // startWidth
            // 
            this.startWidth.Location = new System.Drawing.Point(132, 25);
            this.startWidth.Maximum = 5;
            this.startWidth.Minimum = 1;
            this.startWidth.Name = "startWidth";
            this.startWidth.Size = new System.Drawing.Size(104, 45);
            this.startWidth.TabIndex = 4;
            this.startWidth.Value = 1;
            this.startWidth.Scroll += new System.EventHandler(this.startWidth_Scroll);
            // 
            // labelStartWidth
            // 
            this.labelStartWidth.AutoSize = true;
            this.labelStartWidth.Location = new System.Drawing.Point(36, 25);
            this.labelStartWidth.Name = "labelStartWidth";
            this.labelStartWidth.Size = new System.Drawing.Size(60, 13);
            this.labelStartWidth.TabIndex = 5;
            this.labelStartWidth.Text = "Start Width";
            // 
            // labelStartWidthValue
            // 
            this.labelStartWidthValue.AutoSize = true;
            this.labelStartWidthValue.Location = new System.Drawing.Point(243, 25);
            this.labelStartWidthValue.Name = "labelStartWidthValue";
            this.labelStartWidthValue.Size = new System.Drawing.Size(13, 13);
            this.labelStartWidthValue.TabIndex = 6;
            this.labelStartWidthValue.Text = "1";
            // 
            // labelShiftValue
            // 
            this.labelShiftValue.AutoSize = true;
            this.labelShiftValue.Location = new System.Drawing.Point(243, 76);
            this.labelShiftValue.Name = "labelShiftValue";
            this.labelShiftValue.Size = new System.Drawing.Size(13, 13);
            this.labelShiftValue.TabIndex = 9;
            this.labelShiftValue.Text = "1";
            // 
            // labelShift
            // 
            this.labelShift.AutoSize = true;
            this.labelShift.Location = new System.Drawing.Point(36, 76);
            this.labelShift.Name = "labelShift";
            this.labelShift.Size = new System.Drawing.Size(28, 13);
            this.labelShift.TabIndex = 8;
            this.labelShift.Text = "Shift";
            // 
            // shift
            // 
            this.shift.Location = new System.Drawing.Point(132, 76);
            this.shift.Maximum = 5;
            this.shift.Name = "shift";
            this.shift.Size = new System.Drawing.Size(104, 45);
            this.shift.TabIndex = 7;
            this.shift.Value = 1;
            this.shift.Scroll += new System.EventHandler(this.shift_Scroll);
            // 
            // labelIncreaseValue
            // 
            this.labelIncreaseValue.AutoSize = true;
            this.labelIncreaseValue.Location = new System.Drawing.Point(243, 127);
            this.labelIncreaseValue.Name = "labelIncreaseValue";
            this.labelIncreaseValue.Size = new System.Drawing.Size(13, 13);
            this.labelIncreaseValue.TabIndex = 12;
            this.labelIncreaseValue.Text = "1";
            // 
            // labelIncrease
            // 
            this.labelIncrease.AutoSize = true;
            this.labelIncrease.Location = new System.Drawing.Point(36, 127);
            this.labelIncrease.Name = "labelIncrease";
            this.labelIncrease.Size = new System.Drawing.Size(48, 13);
            this.labelIncrease.TabIndex = 11;
            this.labelIncrease.Text = "Increase";
            // 
            // increase
            // 
            this.increase.Location = new System.Drawing.Point(132, 127);
            this.increase.Maximum = 5;
            this.increase.Name = "increase";
            this.increase.Size = new System.Drawing.Size(104, 45);
            this.increase.TabIndex = 10;
            this.increase.Value = 1;
            this.increase.Scroll += new System.EventHandler(this.increase_Scroll);
            // 
            // checkBoxVertical
            // 
            this.checkBoxVertical.AutoSize = true;
            this.checkBoxVertical.Checked = true;
            this.checkBoxVertical.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVertical.Location = new System.Drawing.Point(21, 20);
            this.checkBoxVertical.Name = "checkBoxVertical";
            this.checkBoxVertical.Size = new System.Drawing.Size(61, 17);
            this.checkBoxVertical.TabIndex = 13;
            this.checkBoxVertical.Text = "Vertical";
            this.checkBoxVertical.UseVisualStyleBackColor = true;
            this.checkBoxVertical.CheckedChanged += new System.EventHandler(this.checkBoxVertical_CheckedChanged);
            // 
            // checkBoxHorizontal
            // 
            this.checkBoxHorizontal.AutoSize = true;
            this.checkBoxHorizontal.Checked = true;
            this.checkBoxHorizontal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHorizontal.Location = new System.Drawing.Point(21, 44);
            this.checkBoxHorizontal.Name = "checkBoxHorizontal";
            this.checkBoxHorizontal.Size = new System.Drawing.Size(73, 17);
            this.checkBoxHorizontal.TabIndex = 14;
            this.checkBoxHorizontal.Text = "Horizontal";
            this.checkBoxHorizontal.UseVisualStyleBackColor = true;
            this.checkBoxHorizontal.CheckedChanged += new System.EventHandler(this.checkBoxHorizontal_CheckedChanged);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelResult.Location = new System.Drawing.Point(422, 124);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(52, 16);
            this.labelResult.TabIndex = 16;
            this.labelResult.Text = "Result";
            this.labelResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelXDimension
            // 
            this.labelXDimension.AutoSize = true;
            this.labelXDimension.Location = new System.Drawing.Point(683, 12);
            this.labelXDimension.Name = "labelXDimension";
            this.labelXDimension.Size = new System.Drawing.Size(14, 13);
            this.labelXDimension.TabIndex = 17;
            this.labelXDimension.Text = "X";
            // 
            // labelYDimension
            // 
            this.labelYDimension.AutoSize = true;
            this.labelYDimension.Location = new System.Drawing.Point(1092, 89);
            this.labelYDimension.Name = "labelYDimension";
            this.labelYDimension.Size = new System.Drawing.Size(14, 13);
            this.labelYDimension.TabIndex = 18;
            this.labelYDimension.Text = "Y";
            // 
            // startX
            // 
            this.startX.Location = new System.Drawing.Point(703, 57);
            this.startX.Maximum = 6;
            this.startX.Name = "startX";
            this.startX.Size = new System.Drawing.Size(104, 45);
            this.startX.TabIndex = 19;
            this.startX.Scroll += new System.EventHandler(this.startX_Scroll);
            // 
            // labelStartX
            // 
            this.labelStartX.AutoSize = true;
            this.labelStartX.Location = new System.Drawing.Point(658, 57);
            this.labelStartX.Name = "labelStartX";
            this.labelStartX.Size = new System.Drawing.Size(39, 13);
            this.labelStartX.TabIndex = 20;
            this.labelStartX.Text = "Start X";
            // 
            // labelStartXValue
            // 
            this.labelStartXValue.AutoSize = true;
            this.labelStartXValue.Location = new System.Drawing.Point(813, 60);
            this.labelStartXValue.Name = "labelStartXValue";
            this.labelStartXValue.Size = new System.Drawing.Size(13, 13);
            this.labelStartXValue.TabIndex = 21;
            this.labelStartXValue.Text = "0";
            // 
            // labelEndXValue
            // 
            this.labelEndXValue.AutoSize = true;
            this.labelEndXValue.Location = new System.Drawing.Point(991, 60);
            this.labelEndXValue.Name = "labelEndXValue";
            this.labelEndXValue.Size = new System.Drawing.Size(13, 13);
            this.labelEndXValue.TabIndex = 24;
            this.labelEndXValue.Text = "6";
            // 
            // labelEndX
            // 
            this.labelEndX.AutoSize = true;
            this.labelEndX.Location = new System.Drawing.Point(836, 57);
            this.labelEndX.Name = "labelEndX";
            this.labelEndX.Size = new System.Drawing.Size(36, 13);
            this.labelEndX.TabIndex = 23;
            this.labelEndX.Text = "End X";
            // 
            // endX
            // 
            this.endX.Location = new System.Drawing.Point(881, 57);
            this.endX.Maximum = 6;
            this.endX.Name = "endX";
            this.endX.Size = new System.Drawing.Size(104, 45);
            this.endX.TabIndex = 22;
            this.endX.Value = 6;
            this.endX.Scroll += new System.EventHandler(this.endX_Scroll);
            // 
            // endY
            // 
            this.endY.Location = new System.Drawing.Point(1023, 264);
            this.endY.Maximum = 3;
            this.endY.Name = "endY";
            this.endY.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.endY.Size = new System.Drawing.Size(45, 104);
            this.endY.TabIndex = 25;
            this.endY.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.endY.Value = 3;
            this.endY.Scroll += new System.EventHandler(this.endY_Scroll);
            // 
            // labelEndY
            // 
            this.labelEndY.AutoSize = true;
            this.labelEndY.Location = new System.Drawing.Point(1020, 248);
            this.labelEndY.Name = "labelEndY";
            this.labelEndY.Size = new System.Drawing.Size(36, 13);
            this.labelEndY.TabIndex = 26;
            this.labelEndY.Text = "End Y";
            // 
            // labelEndYValue
            // 
            this.labelEndYValue.AutoSize = true;
            this.labelEndYValue.Location = new System.Drawing.Point(1035, 371);
            this.labelEndYValue.Name = "labelEndYValue";
            this.labelEndYValue.Size = new System.Drawing.Size(13, 13);
            this.labelEndYValue.TabIndex = 27;
            this.labelEndYValue.Text = "3";
            // 
            // labelStartYValue
            // 
            this.labelStartYValue.AutoSize = true;
            this.labelStartYValue.Location = new System.Drawing.Point(1035, 221);
            this.labelStartYValue.Name = "labelStartYValue";
            this.labelStartYValue.Size = new System.Drawing.Size(13, 13);
            this.labelStartYValue.TabIndex = 30;
            this.labelStartYValue.Text = "0";
            // 
            // labelStartY
            // 
            this.labelStartY.AutoSize = true;
            this.labelStartY.Location = new System.Drawing.Point(1020, 98);
            this.labelStartY.Name = "labelStartY";
            this.labelStartY.Size = new System.Drawing.Size(39, 13);
            this.labelStartY.TabIndex = 29;
            this.labelStartY.Text = "Start Y";
            // 
            // startY
            // 
            this.startY.Location = new System.Drawing.Point(1023, 114);
            this.startY.Maximum = 3;
            this.startY.Name = "startY";
            this.startY.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.startY.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.startY.Size = new System.Drawing.Size(45, 104);
            this.startY.TabIndex = 28;
            this.startY.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.startY.Scroll += new System.EventHandler(this.startY_Scroll);
            // 
            // radioButtonXLeft
            // 
            this.radioButtonXLeft.AutoSize = true;
            this.radioButtonXLeft.Enabled = false;
            this.radioButtonXLeft.Location = new System.Drawing.Point(6, 19);
            this.radioButtonXLeft.Name = "radioButtonXLeft";
            this.radioButtonXLeft.Size = new System.Drawing.Size(43, 17);
            this.radioButtonXLeft.TabIndex = 31;
            this.radioButtonXLeft.TabStop = true;
            this.radioButtonXLeft.Text = "Left";
            this.radioButtonXLeft.UseVisualStyleBackColor = true;
            this.radioButtonXLeft.CheckedChanged += new System.EventHandler(this.radioButtonXLeft_CheckedChanged);
            // 
            // radioButtonXRight
            // 
            this.radioButtonXRight.AutoSize = true;
            this.radioButtonXRight.Enabled = false;
            this.radioButtonXRight.Location = new System.Drawing.Point(55, 19);
            this.radioButtonXRight.Name = "radioButtonXRight";
            this.radioButtonXRight.Size = new System.Drawing.Size(50, 17);
            this.radioButtonXRight.TabIndex = 32;
            this.radioButtonXRight.TabStop = true;
            this.radioButtonXRight.Text = "Right";
            this.radioButtonXRight.UseVisualStyleBackColor = true;
            this.radioButtonXRight.CheckedChanged += new System.EventHandler(this.radioButtonXRight_CheckedChanged);
            // 
            // radioButtonYUp
            // 
            this.radioButtonYUp.AutoSize = true;
            this.radioButtonYUp.Enabled = false;
            this.radioButtonYUp.Location = new System.Drawing.Point(6, 19);
            this.radioButtonYUp.Name = "radioButtonYUp";
            this.radioButtonYUp.Size = new System.Drawing.Size(39, 17);
            this.radioButtonYUp.TabIndex = 33;
            this.radioButtonYUp.TabStop = true;
            this.radioButtonYUp.Text = "Up";
            this.radioButtonYUp.UseVisualStyleBackColor = true;
            this.radioButtonYUp.CheckedChanged += new System.EventHandler(this.radioButtonYUp_CheckedChanged);
            // 
            // radioButtonYDown
            // 
            this.radioButtonYDown.AutoSize = true;
            this.radioButtonYDown.Enabled = false;
            this.radioButtonYDown.Location = new System.Drawing.Point(55, 19);
            this.radioButtonYDown.Name = "radioButtonYDown";
            this.radioButtonYDown.Size = new System.Drawing.Size(53, 17);
            this.radioButtonYDown.TabIndex = 34;
            this.radioButtonYDown.TabStop = true;
            this.radioButtonYDown.Text = "Down";
            this.radioButtonYDown.UseVisualStyleBackColor = true;
            this.radioButtonYDown.CheckedChanged += new System.EventHandler(this.radioButtonYDown_CheckedChanged);
            // 
            // groupBoxDirectionX
            // 
            this.groupBoxDirectionX.Controls.Add(this.checkBoxXAuto);
            this.groupBoxDirectionX.Controls.Add(this.radioButtonXLeft);
            this.groupBoxDirectionX.Controls.Add(this.radioButtonXRight);
            this.groupBoxDirectionX.Location = new System.Drawing.Point(39, 248);
            this.groupBoxDirectionX.Name = "groupBoxDirectionX";
            this.groupBoxDirectionX.Size = new System.Drawing.Size(205, 47);
            this.groupBoxDirectionX.TabIndex = 35;
            this.groupBoxDirectionX.TabStop = false;
            this.groupBoxDirectionX.Text = "Draw direction X";
            // 
            // checkBoxXAuto
            // 
            this.checkBoxXAuto.AutoSize = true;
            this.checkBoxXAuto.Checked = true;
            this.checkBoxXAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxXAuto.Location = new System.Drawing.Point(117, 20);
            this.checkBoxXAuto.Name = "checkBoxXAuto";
            this.checkBoxXAuto.Size = new System.Drawing.Size(88, 17);
            this.checkBoxXAuto.TabIndex = 33;
            this.checkBoxXAuto.Text = "Automatically";
            this.checkBoxXAuto.UseVisualStyleBackColor = true;
            this.checkBoxXAuto.CheckedChanged += new System.EventHandler(this.checkBoxXAuto_CheckedChanged);
            // 
            // groupBoxDirectionY
            // 
            this.groupBoxDirectionY.Controls.Add(this.checkBoxYAuto);
            this.groupBoxDirectionY.Controls.Add(this.radioButtonYUp);
            this.groupBoxDirectionY.Controls.Add(this.radioButtonYDown);
            this.groupBoxDirectionY.Location = new System.Drawing.Point(39, 301);
            this.groupBoxDirectionY.Name = "groupBoxDirectionY";
            this.groupBoxDirectionY.Size = new System.Drawing.Size(205, 50);
            this.groupBoxDirectionY.TabIndex = 36;
            this.groupBoxDirectionY.TabStop = false;
            this.groupBoxDirectionY.Text = "Draw direction Y";
            // 
            // checkBoxYAuto
            // 
            this.checkBoxYAuto.AutoSize = true;
            this.checkBoxYAuto.Checked = true;
            this.checkBoxYAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxYAuto.Location = new System.Drawing.Point(117, 20);
            this.checkBoxYAuto.Name = "checkBoxYAuto";
            this.checkBoxYAuto.Size = new System.Drawing.Size(88, 17);
            this.checkBoxYAuto.TabIndex = 35;
            this.checkBoxYAuto.Text = "Automatically";
            this.checkBoxYAuto.UseVisualStyleBackColor = true;
            this.checkBoxYAuto.CheckedChanged += new System.EventHandler(this.checkBoxYAuto_CheckedChanged);
            // 
            // groupBoxReflection
            // 
            this.groupBoxReflection.Controls.Add(this.checkBoxVertical);
            this.groupBoxReflection.Controls.Add(this.checkBoxHorizontal);
            this.groupBoxReflection.Location = new System.Drawing.Point(69, 167);
            this.groupBoxReflection.Name = "groupBoxReflection";
            this.groupBoxReflection.Size = new System.Drawing.Size(136, 75);
            this.groupBoxReflection.TabIndex = 37;
            this.groupBoxReflection.TabStop = false;
            this.groupBoxReflection.Text = "Reflection";
            // 
            // DrawForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 688);
            this.Controls.Add(this.groupBoxReflection);
            this.Controls.Add(this.groupBoxDirectionY);
            this.Controls.Add(this.groupBoxDirectionX);
            this.Controls.Add(this.labelStartYValue);
            this.Controls.Add(this.labelStartY);
            this.Controls.Add(this.startY);
            this.Controls.Add(this.labelEndYValue);
            this.Controls.Add(this.labelEndY);
            this.Controls.Add(this.endY);
            this.Controls.Add(this.labelEndXValue);
            this.Controls.Add(this.labelEndX);
            this.Controls.Add(this.endX);
            this.Controls.Add(this.labelStartXValue);
            this.Controls.Add(this.labelStartX);
            this.Controls.Add(this.startX);
            this.Controls.Add(this.labelYDimension);
            this.Controls.Add(this.labelXDimension);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelIncreaseValue);
            this.Controls.Add(this.labelIncrease);
            this.Controls.Add(this.increase);
            this.Controls.Add(this.labelShiftValue);
            this.Controls.Add(this.labelShift);
            this.Controls.Add(this.shift);
            this.Controls.Add(this.labelStartWidthValue);
            this.Controls.Add(this.labelStartWidth);
            this.Controls.Add(this.startWidth);
            this.Controls.Add(this.labelFieldDimY);
            this.Controls.Add(this.fieldDimY);
            this.Controls.Add(this.labelFieldDimX);
            this.Controls.Add(this.fieldDimX);
            this.Name = "DrawForm";
            this.Text = "GIVE ME MY BEER";
            ((System.ComponentModel.ISupportInitialize)(this.fieldDimX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldDimY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.increase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startY)).EndInit();
            this.groupBoxDirectionX.ResumeLayout(false);
            this.groupBoxDirectionX.PerformLayout();
            this.groupBoxDirectionY.ResumeLayout(false);
            this.groupBoxDirectionY.PerformLayout();
            this.groupBoxReflection.ResumeLayout(false);
            this.groupBoxReflection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar fieldDimX;
        private System.Windows.Forms.Label labelFieldDimX;
        private System.Windows.Forms.Label labelFieldDimY;
        private System.Windows.Forms.TrackBar fieldDimY;
        private System.Windows.Forms.TrackBar startWidth;
        private System.Windows.Forms.Label labelStartWidth;
        private System.Windows.Forms.Label labelStartWidthValue;
        private System.Windows.Forms.Label labelShiftValue;
        private System.Windows.Forms.Label labelShift;
        private System.Windows.Forms.TrackBar shift;
        private System.Windows.Forms.Label labelIncreaseValue;
        private System.Windows.Forms.Label labelIncrease;
        private System.Windows.Forms.TrackBar increase;
        private System.Windows.Forms.CheckBox checkBoxVertical;
        private System.Windows.Forms.CheckBox checkBoxHorizontal;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelXDimension;
        private System.Windows.Forms.Label labelYDimension;
        private System.Windows.Forms.TrackBar startX;
        private System.Windows.Forms.Label labelStartX;
        private System.Windows.Forms.Label labelStartXValue;
        private System.Windows.Forms.Label labelEndXValue;
        private System.Windows.Forms.Label labelEndX;
        private System.Windows.Forms.TrackBar endX;
        private System.Windows.Forms.TrackBar endY;
        private System.Windows.Forms.Label labelEndY;
        private System.Windows.Forms.Label labelEndYValue;
        private System.Windows.Forms.Label labelStartYValue;
        private System.Windows.Forms.Label labelStartY;
        private System.Windows.Forms.TrackBar startY;
        private System.Windows.Forms.RadioButton radioButtonXLeft;
        private System.Windows.Forms.RadioButton radioButtonXRight;
        private System.Windows.Forms.RadioButton radioButtonYUp;
        private System.Windows.Forms.RadioButton radioButtonYDown;
        private System.Windows.Forms.GroupBox groupBoxDirectionX;
        private System.Windows.Forms.GroupBox groupBoxDirectionY;
        private System.Windows.Forms.CheckBox checkBoxXAuto;
        private System.Windows.Forms.CheckBox checkBoxYAuto;
        private System.Windows.Forms.GroupBox groupBoxReflection;
    }
}

