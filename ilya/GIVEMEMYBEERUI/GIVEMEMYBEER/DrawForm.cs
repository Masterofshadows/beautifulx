﻿using System;
using System.Windows.Forms;

namespace GIVEMEMYBEER
{
    public partial class DrawForm : Form
    {
        public DrawForm()
        {
            InitializeComponent();
            RefreshForm();
        }

        private void RefreshForm()
        {
            labelResult.Text = DrawUtils.Draw(fieldDimX.Value * 2, fieldDimY.Value * 2, startWidth.Value, shift.Value, increase.Value,
                checkBoxVertical.Checked, checkBoxHorizontal.Checked, startX.Value, startY.Value, endX.Value, endY.Value, GetDirectionX(), GetDirectionY());
            UpdateDrawDirections();
        }

        private void UpdateDrawDirections()
        {
            if (checkBoxXAuto.Checked)
            {
                if (GetDirectionX())
                {
                    radioButtonXRight.Checked = true;
                }
                else
                {
                    radioButtonXLeft.Checked = true;
                }
            }

            if (checkBoxYAuto.Checked)
            {
                if (GetDirectionY())
                {
                    radioButtonYDown.Checked = true;
                }
                else
                {
                    radioButtonYUp.Checked = true;
                }
            }
        }

        private bool GetDirectionX()
        {
            return checkBoxXAuto.Checked ? startX.Value <= endX.Value : radioButtonXRight.Checked;
        }

        private bool GetDirectionY()
        {
            return checkBoxYAuto.Checked ? startY.Value <= endY.Value : radioButtonYDown.Checked;
        }

        private void fieldDimX_Scroll(object sender, EventArgs e)
        {
            labelFieldDimX.Text = (fieldDimX.Value * 2).ToString();
            startX.Maximum = (fieldDimX.Value - 1) * (checkBoxVertical.Checked ? 1 : 2);
            endX.Maximum = (fieldDimX.Value - 1) * (checkBoxVertical.Checked ? 1 : 2);
            labelStartXValue.Text = startX.Value.ToString();
            labelEndXValue.Text = endX.Value.ToString();
            RefreshForm();
        }

        private void fieldDimY_Scroll(object sender, EventArgs e)
        {
            labelFieldDimY.Text = (fieldDimY.Value * 2).ToString();
            startY.Maximum = (fieldDimY.Value - 1) * (checkBoxHorizontal.Checked ? 1 : 2);
            endY.Maximum = (fieldDimY.Value - 1) * (checkBoxHorizontal.Checked ? 1 : 2);
            labelStartYValue.Text = startY.Value.ToString();
            labelEndYValue.Text = endY.Value.ToString();
            RefreshForm();
        }

        private void startX_Scroll(object sender, EventArgs e)
        {
            labelStartXValue.Text = startX.Value.ToString();
            RefreshForm();
        }

        private void endX_Scroll(object sender, EventArgs e)
        {
            labelEndXValue.Text = endX.Value.ToString();
            RefreshForm();
        }

        private void startY_Scroll(object sender, EventArgs e)
        {
            labelStartYValue.Text = startY.Value.ToString();
            RefreshForm();
        }

        private void endY_Scroll(object sender, EventArgs e)
        {
            labelEndYValue.Text = endY.Value.ToString();
            RefreshForm();
        }

        private void startWidth_Scroll(object sender, EventArgs e)
        {
            labelStartWidthValue.Text = startWidth.Value.ToString();
            RefreshForm();
        }

        private void shift_Scroll(object sender, EventArgs e)
        {
            labelShiftValue.Text = shift.Value.ToString();
            RefreshForm();
        }

        private void increase_Scroll(object sender, EventArgs e)
        {
            labelIncreaseValue.Text = increase.Value.ToString();
            RefreshForm();
        }

        private void checkBoxVertical_CheckedChanged(object sender, EventArgs e)
        {
            startX.Maximum = (fieldDimX.Value - 1) * 2;
            endX.Maximum = (fieldDimX.Value - 1) * 2;
            RefreshForm();
        }

        private void checkBoxHorizontal_CheckedChanged(object sender, EventArgs e)
        {
            startY.Maximum = (fieldDimY.Value - 1) * 2;
            endY.Maximum = (fieldDimY.Value - 1) * 2;
            RefreshForm();
        }

        private void checkBoxXAuto_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonXLeft.Enabled = !checkBoxXAuto.Checked;
            radioButtonXRight.Enabled = !checkBoxXAuto.Checked;
            UpdateDrawDirections();
        }

        private void checkBoxYAuto_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonYUp.Enabled = !checkBoxYAuto.Checked;
            radioButtonYDown.Enabled = !checkBoxYAuto.Checked;
            UpdateDrawDirections();
        }

        private void radioButtonXLeft_CheckedChanged(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void radioButtonXRight_CheckedChanged(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void radioButtonYUp_CheckedChanged(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void radioButtonYDown_CheckedChanged(object sender, EventArgs e)
        {
            RefreshForm();
        }
    }
}
