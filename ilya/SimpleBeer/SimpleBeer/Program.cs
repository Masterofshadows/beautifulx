﻿using System;
using System.Linq;

namespace SimpleBeer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            char[,] field = new char[8, 14];
            for (int y = 0; y < field.GetLength(0); y++)
            {
                for (int x = 0; x < field.GetLength(1); x++)
                {
                    if (y < field.GetLength(0) / 2 && x < field.GetLength(1) / 2)
                    {
                        field[y, x] = Enumerable.Range(y, y + 1).Contains(x) ? '#' : ' ';
                        field[y, field.GetLength(1) - 1 - x] = field[field.GetLength(0) - 1 - y, x] = field[field.GetLength(0) - 1 - y, field.GetLength(1) - 1 - x] = field[y, x];
                    }
                    Console.Write($"{field[y, x]}{(x == field.GetLength(1) - 1 ? "\n" : "")}");
                }
            }
            Console.ReadLine();
        }
    }
}