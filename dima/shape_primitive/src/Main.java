public class Main {

    public static void main(String[] args) {

        for (int y = 7; y >= -7; y -= 2) {
            for (int x = 13; x >= -15; x -= 2) {
                System.out.print(((13 - Math.abs(x)) / 2) < 0 ? '\n' : ((13 - Math.abs(x)) / 2) >= (7 - Math.abs(y)) / 2 && ((13 - Math.abs(x)) / 2) <= (7 - Math.abs(y)) / 2 * 2 ? '#' : ' ');
            }
        }
    }
}
