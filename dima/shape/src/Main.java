public class Main {

    public static void main(String[] args) {

        for (int y = 7; y >= -7; y -= 2) {
            for (int x = 13; x >= -15; x -= 2) {
                System.out.print(getChar(((13 - Math.abs(x)) / 2), (7 - Math.abs(y)) / 2));
            }
        }
    }

    //Number of lines can be reduced by 2 if you move logic from getChar into System.out.print.
    //But code looks prettier with this method.
    private static char getChar(int x, int y) {
        return x < 0 ? '\n' : x >= y && x <= y * 2 ? '#' : ' ';
    }
}
